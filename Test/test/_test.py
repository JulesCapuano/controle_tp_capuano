# -*- coding: utf-8 -*-
from Robot.Map.Map import Grid
import unittest
import numpy as np

class DownTest(unittest.TestCase):
    def setUp(self):
        self.grid = Grid(3,3)
        
    def test_down(self):
        result = self.grid.down()
        self.assertEqual(result, "True")
    
    def test_up(self):
        result = self.grid.up()
        self.assertEqual(result, "False")
        
class RightTest(unittest.TestCase):
    def setUp(self):
        self.grid = Grid(3,3)
        
    def test_right(self):
        result = self.grid.right()
        self.assertEqual(result,"True")
        
if __name__ == "__main__":
    unittest.main()