# -*- coding: utf-8 -*-
import numpy as np

class Grid:
    def __init__(self,nb_lines,nb_colums):
        self.nb_lines = nb_lines
        self.nb_colums = nb_colums
        self.grid = np.zeros(shape=(nb_lines, nb_colums))
        self.grid[0][0] = 1
        self.index = np.nonzero(self.grid)

    def Print(self):
        print(self.grid)

    def up(self):
        if self.index[0][0] != 0:
            return("True")
            self.grid[self.index[0][0]][self.index[1][0]],self.grid[self.index[0][0]-1][self.index[1][0]]=self.grid[self.index[0][0]-1][self.index[1][0]],self.grid[self.index[0][0]][self.index[1][0]]
            self.index = np.nonzero(self.grid)
        else:
            return('False') 

    def down(self):
        if self.index[0][0] != self.nb_lines-1:
            return("True")
            self.grid[self.index[0][0]][self.index[1][0]], self.grid[self.index[0][0]+1][self.index[1][0]] = self.grid[self.index[0][0]+1][self.index[1][0]], self.grid[self.index[0][0]][self.index[1][0]]
            self.index = np.nonzero(self.grid)
        else:
            return('False')

    def right(self):
        if self.index[1][0] != self.nb_colums-1:
            return("True")
            self.grid[self.index[0][0]][self.index[1][0]], self.grid[self.index[0][0]][self.index[1][0]+1] = self.grid[self.index[0][0]][self.index[1][0]+1], self.grid[self.index[0][0]][self.index[1][0]]
            self.index = np.nonzero(self.grid)
        else:
            return('False') 

    def left(self):
        if self.index[1][0] != 0:
            return("True")
            self.grid[self.index[0][0]][self.index[1][0]], self.grid[self.index[0][0]][self.index[1][0]-1] = self.grid[self.index[0][0]][self.index[1][0]-1], self.grid[self.index[0][0]][self.index[1][0]]
            self.index = np.nonzero(self.grid)
        else:
            return('False')

        
        