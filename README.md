### Projet Map

Afin de réaliser ce projet j'ai décidé d'utiliser des matrices et donc la librairie numpy.

## Création Map.py

En premier lieu il fallait créer une matrice de taille choisit par l'utilisateur on utilise donc la méthode _zero_ de numpy 
qui prend en paramètre le nombre de lignes et de colonnes et qui crée une matrice de zero.<br>
Ensuite on place un 1 en (0,0) qui simule la position du robot.<br>
Puis on utilise la méthode _nonzero_ qui permet de renvoyer dans un Tuple les indices de lignes puis de colonnes des valeurs différentes de zéro de la matrice qui l'on passe en paramètre ici : _self.grid_
afin d'avoir la postion du robot dans la variable index.<br>
Ensuite pour les différentes méthode _up_ _down_ _right_ _left_ on interverti la position du 1 avec la valeur respective du dessus, du dessous, de droite ou de gauche.<br>
Enfin on oublie pas de réactualiser la variable index, sinon la position du robot ne changera pas et on ne pourra effectuer qu'un seul déplacement puis on crée un condition afin que le robot ne puisse pas se déplacer
si celui-ci est au bord.<br> <br>

## Test

Les tests que j'ai lancé son plutot simple et n'implique pas beaucoup de déplacement, manque de temps j'ai préféré faire quelque chose de simple mais fonctionnel.<br>
En revanche on aurait pu créer des tests qui permettent de vérifier la position du robot après plusieurs déplacement, à l'aide de la méthode _nonzero_.

## Intégration continue
Pour l'intégration continue on oublie pas de rajouter _numpy == 1.18.4_ dans le requirement.txt afin que gitlab puisse l'utiliser.

